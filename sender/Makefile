CC           = gcc

CFLAGS       = -c -g -W -Wall -Wmissing-declarations -Wmissing-prototypes
CFLAGS      += -Wredundant-decls -Wshadow -Wbad-function-cast -Wcast-qual
CFLAGS      += -Werror

LDFLAGS_NET  =

BIN = sender
CPATH = src/
OPATH = obj/
BPATH = bin/
DEPPATH = deps/
HPATH = include/
INCLUDE = -I$(HPATH)
CFILES = $(wildcard $(CPATH)*.c)
OFILES = $(patsubst $(CPATH)%.c, $(OPATH)%.o, $(CFILES))
DEPS = $(patsubst $(CPATH)%.c, $(DEPPATH)%.d, $(CFILES))
COMMON_PATH = ../common/
COMMON_CPATH = $(COMMON_PATH)src/
COMMON_OPATH = $(COMMON_PATH)obj/
COMMON_DPATH = $(COMMON_PATH)deps/
COMMON_HPATH = $(COMMON_PATH)include/
COMMON_INCLUDE = -I$(COMMON_HPATH)
COMMON_CFILES = $(wildcard $(COMMON_CPATH)*.c)
COMMON_OFILES = $(patsubst $(COMMON_CPATH)%.c, $(COMMON_OPATH)%.o, $(COMMON_CFILES))
COMMON_DEPS = $(patsubst $(COMMON_CPATH)%.c, $(COMMON_DPATH)%.d, $(COMMON_CFILES))

ARGS = $(CPATH)sending.c

SOCK_DIR = /tmp/
SOCKETS = $(SOCK_DIR)sock_send

ifeq ($(CPATH)coding2.c, $(wildcard $(CPATH)coding2.c))
  BIN += sender2
endif


# Color definition
COLOR_DEFAULT    = \e[39m
COLOR_BLACK      = \e[30m 
COLOR_RED        = \e[31m
COLOR_GREEN      = \e[32m
COLOR_YELLOW     = \e[33m
COLOR_BLUE       = \e[34m
COLOR_MAGENTA    = \e[35m
COLOR_CYAN       = \e[36m


.PHONY: sockclean clean mrproper run


# main
$(BPATH)$(BIN) : $(COMMON_OFILES) $(OFILES)
	@mkdir -p $(BPATH)
	@echo "$(COLOR_CYAN)---- LINKING ----$(COLOR_DEFAULT)"
	$(CC) -o $@ $^ $(LDFLAGS_NET)


# Generating dependencies
$(COMMON_DPATH)%.d: $(COMMON_CPATH)%.c
	@mkdir -p $(COMMON_DPATH)
	@echo "$(COLOR_RED)---- GENERATING DEPENDENCIES FOR $< ----$(COLOR_DEFAULT)"
	@echo -n "$(COMMON_OPATH)" > $@
	@$(CC) $(CFLAGS) $(COMMON_INCLUDE) -MM $< >> $@

$(DEPPATH)%.d: $(CPATH)%.c
	@mkdir -p $(DEPPATH)
	@echo "$(COLOR_RED)---- GENERATING DEPENDENCIES FOR $< ----$(COLOR_DEFAULT)"
	@echo -n "$(OPATH)" > $@
	@$(CC) $(CFLAGS) $(INCLUDE) $(COMMON_INCLUDE) -MM $< >> $@


# Include generated dependencies
include $(DEPS)
include $(COMMON_DEPS)


$(COMMON_OPATH)%.o: $(COMMON_CPATH)%.c
	@mkdir -p $(COMMON_OPATH)
	@echo "$(COLOR_YELLOW)---- COMPILING $@ ----$(COLOR_DEFAULT)"
	@$(CC) $(CFLAGS) -c $< $(COMMON_INCLUDE) -o $@

$(OPATH)%.o: $(CPATH)%.c
	@mkdir -p $(OPATH)
	@echo "$(COLOR_YELLOW)---- COMPILING $@ ----$(COLOR_DEFAULT)"
	@$(CC) $(CFLAGS) -c $< $(INCLUDE) $(COMMON_INCLUDE) -o $@


run: $(BPATH)$(BIN) sockclean
	@echo "\n$(COLOR_MAGENTA)---- RUNNING $(BPATH)$(BIN) ----$(COLOR_DEFAULT)"
	./$(BPATH)$(BIN) $(ARGS)


sockclean:
	rm -f $(SOCKETS)

clean: sockclean
	rm -vrf $(OPATH) $(DEPPATH)

mrproper: clean
	rm -vrf $(BPATH)
	rm -vrf $(COMMON_OPATH) $(COMMON_DPATH)