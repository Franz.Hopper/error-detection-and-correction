/**
 * @file coding.c
 * @author Arash Habibi
 * @author Julien Montavont
 * @version 2.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Generate code words from the initial data flow
 */

#include "codingdecoding.h"

void copyDataBitsCoding(char *message, CodeWord_t *cw, int size)
{
  int i = 0;

  for(i=0; i<size; i++)
  {
    setNthBitCW(&(cw[i]), 1, getNthBit(message[i], 1));
    setNthBitCW(&(cw[i]), 2, getNthBit(message[i], 2));
    setNthBitCW(&(cw[i]), 3, getNthBit(message[i], 3));
    setNthBitCW(&(cw[i]), 4, getNthBit(message[i], 4));
    setNthBitCW(&(cw[i]), 5, getNthBit(message[i], 5));
    setNthBitCW(&(cw[i]), 6, getNthBit(message[i], 6));
    setNthBitCW(&(cw[i]), 7, getNthBit(message[i], 7));
    setNthBitCW(&(cw[i]), 8, getNthBit(message[i], 8));

    // Print
    // printBits(message[i], "orig");
  }

  return;
}


const char G[4][8] = {{1,0,0,0,1,0,1,1},
                      {0,1,0,0,0,1,1,1},
                      {0,0,1,0,1,1,0,1},
                      {0,0,0,1,1,1,1,0}};

void computeCtrlBits(CodeWord_t *message, int size)
{
  int max = size / sizeof(CodeWord_t);
  char w, val;

  for(int i=0; i<max; i++)
  {
    // Encode first part of the word
    for(int j=0; j<8; j++){
      val = 0;
      for(int k=0; k<4; k++){
        val ^= getNthBit(message[i], k+1) & G[k][j];
      }
      setNthBitW(&w, j+1, val);
    }

    // Set data and control at the right index
    setNthBitCW(&(message[i]), 1, getNthBit(w, 1));
    setNthBitCW(&(message[i]), 2, getNthBit(w, 2));
    setNthBitCW(&(message[i]), 3, getNthBit(w, 3));
    setNthBitCW(&(message[i]), 4, getNthBit(w, 4));
    setNthBitCW(&(message[i]), 9, getNthBit(w, 5));
    setNthBitCW(&(message[i]), 10, getNthBit(w, 6));
    setNthBitCW(&(message[i]), 11, getNthBit(w, 7));
    setNthBitCW(&(message[i]), 12, getNthBit(w, 8));


    // Encode second part of the word
    for(int j=0; j<8; j++){
      val = 0;
      for(int k=0; k<4; k++){
        val ^= getNthBit(message[i], k+1+4) & G[k][j];
      }
      setNthBitW(&w, j+1, val);
    }

    // Set data and control at the right index
    setNthBitCW(&(message[i]), 5, getNthBit(w, 1));
    setNthBitCW(&(message[i]), 6, getNthBit(w, 2));
    setNthBitCW(&(message[i]), 7, getNthBit(w, 3));
    setNthBitCW(&(message[i]), 8, getNthBit(w, 4));
    setNthBitCW(&(message[i]), 13, getNthBit(w, 5));
    setNthBitCW(&(message[i]), 14, getNthBit(w, 6));
    setNthBitCW(&(message[i]), 15, getNthBit(w, 7));
    setNthBitCW(&(message[i]), 16, getNthBit(w, 8));

    // Print
    // printBits(message[i], "sent");
  }

  return;
}


void coding(char *message, int data_size, char *cw, int *cw_size)
{
  *cw_size = data_size * sizeof(CodeWord_t);

  copyDataBitsCoding(message, (CodeWord_t*)cw, data_size);
  computeCtrlBits((CodeWord_t*)cw, *cw_size);

  return;
}
