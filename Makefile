CC           = gcc

LDFLAGS_NET  =

SENDER       = sender/
MEDIUM       = medium/
RECEIVER     = receiver/
COMMONS      = common/
RPATH        = report/

TARNAME      = TP3Network_cor.tgz

MARGS        = 0
SARGS        = src/sending.c
RARGS        = /tmp/aaa


# Color definition
COLOR_DEFAULT    = \e[39m
COLOR_BLACK      = \e[30m 
COLOR_RED        = \e[31m
COLOR_GREEN      = \e[32m
COLOR_YELLOW     = \e[33m
COLOR_BLUE       = \e[34m
COLOR_MAGENTA    = \e[35m
COLOR_CYAN       = \e[36m


.PHONY: clean mrproper sender receiver medium report help

all: sender receiver medium


help:
	@echo "Usage: make [<target>]"
	@echo "\nAvailable argets are:"
	@echo "  all                     (default target) Compile all sender, medium and receiver."
	@echo "  sender                  Compile only the sender."
	@echo "  medium                  Compile only the medium."
	@echo "  receiver                Compile only the receiver."
	@echo "  clean                   Removes most of the files created during compilation."
	@echo "  mrproper                Get a complete clean repository."
	@echo "  dist                    Create a tar named '$(TARNAME)' containing core files in order to distribute the project."
	@echo "  help                    Display this information message."
	@echo "  test [<options=val>]    Launch a quick test. Supported options are the following:"
	@echo "                          MARGS  Options passed to medium to conduces error introduction. (default $(MARGS))"
	@echo "                            0: Doesn't introduce error in the message."
	@echo "                            1: Introduce a 1 bit error in each word."
	@echo "                            2: Let ERROR_RATE decide the error rate (byte granularity)."
	@echo "                            3: Introduce 1 byte error in the whole message."
	@echo "                            4: Let ERROR_RATE decide the error rate (word granularity)."
	@echo "                          RARGS  Options passed to receiver. (default $(RARGS))"
	@echo "                          SARGS  Options passed to sender.   (default $(SARGS))"
	@echo "                          'make test' is equivalent to 'make test MARGS=$(MARGS) SARGS=$(SARGS) RARGS=$(RARGS)'"


sender: 
	@echo "\n\t$(COLOR_BLACK)Makefile parent sender$(COLOR_DEFAULT)"
	@(cd $(SENDER); make)

receiver: 
	@echo "\n\t$(COLOR_BLACK)Makefile parent receiver$(COLOR_DEFAULT)"
	@(cd $(RECEIVER); make)

medium:
	@echo "\n\t$(COLOR_BLACK)Makefile parent medium$(COLOR_DEFAULT)"
	@(cd $(MEDIUM); make)


dist: report
	cp $(RPATH)report.pdf .
	tar cvf - \
	Makefile README.md sujet.pdf report.pdf \
	$(COMMONS)include $(COMMONS)src \
	$(SENDER)src $(SENDER)Makefile \
	$(MEDIUM)include $(MEDIUM)src $(MEDIUM)Makefile \
	$(RECEIVER)src $(RECEIVER)Makefile \
	| gzip > $(TARNAME)
	rm report.pdf


#gmake test with freeBSD
test: all
	killall medium receiver sender || true
	(cd $(MEDIUM); make run ARGS=$(MARGS)) & sleep 1; (cd $(RECEIVER); make run ARGS=$(RARGS)) & sleep 1; (cd $(SENDER); make run ARGS=$(SARGS))
	@echo "\n$(COLOR_BLUE)---- TESTING DIFFERENCES IN FILES ----$(COLOR_DEFAULT)"
	@diff --color $(RARGS) $(SENDER)$(SARGS)
	@echo "$(COLOR_GREEN)**** SUCESS ****$(COLOR_DEFAULT)"


report: 
	(cd $(RPATH); make)


clean:
	@(cd $(SENDER); make clean)
	@(cd $(RECEIVER); make clean)
	@(cd $(MEDIUM); make clean)

mrproper: clean
	@(cd $(SENDER); make mrproper)
	@(cd $(RECEIVER); make mrproper ARGS=$(RARGS))
	@(cd $(MEDIUM); make mrproper)
	@(cd $(RPATH); make mrproper)